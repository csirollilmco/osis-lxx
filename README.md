# OSIS LXX

OSIS XML files of the [Septuagint](https://github.com/openbibles/LXX-Strongs-and-RMA-Codes), using Strong's numbering and Robinson's morph codes.
